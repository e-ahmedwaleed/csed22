
## Fourth Year

Grid World (Artificial Intelligence) [![external-link](/icon.png)](https://bitbucket.org/csed22/grid-world) [Dec 2021]

8 Puzzle (Artificial Intelligence) [![external-link](/icon.png)](https://bitbucket.org/csed22/8-puzzle) [Nov 2021]

Connect 4 (Artificial Intelligence) [![external-link](/icon.png)](https://bitbucket.org/csed22/connect-4) [Nov 2021]

Face Recognition (Data Mining) [![external-link](/icon.png)](https://bitbucket.org/csed22/lab-2-dimensionality-reduction) [Nov 2021]

Data Exploration (Data Mining) [![external-link](/icon.png)](https://bitbucket.org/csed22/lab-1-data-exploration/) [Oct 2021]

---

## Third Year

Bookstore System (Databases) [![external-link](/icon.png)](https://bitbucket.org/csed22/book-order-system) [Jun 2021]

Simple Compiler (Programming) [![external-link](/icon.png)](https://bitbucket.org/csed22/parser-generator) [Jun 2021]

System of LEDs (Arduino) [![external-link](/icon.png)](https://bitbucket.org/csed22/multi-tasking-arduino) [Apr 2021]

Tilt Sensor Circuit (Arduino) [![external-link](/icon.png)](https://bitbucket.org/csed22/tilt-sensor-circuits) [Apr 2021]

Multi-threaded Matrix Multiplication (Operating Systems) [![external-link](/icon.png)](https://bitbucket.org/csed22/matrix-mult-threads) [Nov 2020]

Simple Shell (Operating Systems) [![external-link](/icon.png)](https://bitbucket.org/csed22/simple-shell) [Nov 2020]

---

## Second Year

Root Locus Graph (Linear Control) [![external-link](/icon.png)](https://bitbucket.org/csed22/root-locus) [Jun 2020]

SIC/XE Assembler (Programming) [![external-link](/icon.png)](https://bitbucket.org/csed22/assembler) [Jun 2020]

Signal Flow Graph (Linear Control) [![external-link](/icon.png)](https://bitbucket.org/csed22/signal-flow-graph) [Jun 2020]

Numerical Methods (Numerical Analysis) [![external-link](/icon.png)](https://bitbucket.org/csed22/numerical-roots-interpolation) [May 2020]

Circus of Plates (Programming) [![external-link](/icon.png)](https://bitbucket.org/csed22/cop) [Dec 2019]

DBMS (Programming) [![external-link](/icon.png)](https://bitbucket.org/csed22/simple-dbms) [Dec 2019]

Power Supply Circuit (Circuits) [![external-link](/icon.png)](https://bitbucket.org/csed22/power-supply-circuit) [Dec 2019]

Paint (Programming) [![external-link](/icon.png)](https://bitbucket.org/csed22/paint) [Nov 2019]

VHDL Modules (Digital Systems) [![external-link](/icon.png)](https://bitbucket.org/csed22/vhdl-modules) [Nov 2019]

Calculator (Programming) [![external-link](/icon.png)](https://bitbucket.org/csed22/calculator) [Oct 2019]

---

## First Year

Circuit Simulations (Circuits) [![external-link](/icon.png)](https://bitbucket.org/csed22/circuit-simulation) [May 2019]

Memory Game (Web Development) [![external-link](/icon.png)](https://bitbucket.org/csed22/memory-game) [May 2019]

Minterm Optimizer (Digital Systems) [![external-link](/icon.png)](https://bitbucket.org/csed22/minterm-optimizer) [Apr 2019]

Chess (Programming) [![external-link](/icon.png)](https://bitbucket.org/csed22/chess) [Dec 2018]

---

Icon used: [![external-link](/icon.png)](https://www.shareicon.net/extract-90827)
